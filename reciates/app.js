//app.js
var baseSet = require('utils/baseSet.js');

App({
  onLaunch: function () {
    // 判断ios和android
    wx.getSystemInfo({
      success: res => {
        if (res.platform == "ios") {
          this.globalData.start = true;
        } else if (res.platform == "android") {
          this.globalData.start = false;          
        }
      }
    })
    
  },
  getInfo: function (callback){
    var _this = this;

    // wx.request({
    //   url: 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx5ffdae5615d602f1&response_type=code&scope=snsapi_base&connect_redirect=1#wechat_redirect',
    //   method: "GET",
    //   success: res=>{
    //     console.log(res)
    //   },
    //   fail: function (res) {
    //     console.log(res)
    //   }
    // })



    // 登录获取用户信息
    wx.login({
      success: r => {
        console.log(r)
        wx.getSetting({
          success: e => {
            console.log(e)
            if (e.authSetting['scope.userInfo']) {
              wx.getUserInfo({
                success: s => {
                  console.log(s)
                  var data = {};
                  data.code = r.code;
                  data.nickname = s.userInfo.nickName;
                  data.province = s.userInfo.province;
                  data.headimgurl = s.userInfo.avatarUrl;
                  data.city = s.userInfo.city;
                  data.encryptedData = s.encryptedData;
                  data.iv = s.iv;
                  baseSet.Interface.login(data, function (res) {
                    console.log(res)
                    if (res.data.code == 0) {
                      _this.globalData.userInfo = res.data.data;
                      callback(res.data.data);
                    }
                  })
                  if (this.userInfoReadyCallback) {
                    this.userInfoReadyCallback(s)
                  }
                }
              })
            }else{
              //无授权
              wx.hideLoading()              
              callback(false);
            }
          }
        })
      }
    })
  },
  globalData: {
    // 用户信息
    userInfo: null,
    // 转发title
    title:"【赞帮手】",
    // 设置ios和android
    start:false,
  },
  
  baseSet: function () {
    return require('utils/baseSet.js');
  },
  util:function(){
    return require("utils/util.js");
  }
})