const app = getApp()
const baseSet = app.baseSet().Interface;
const util = app.util();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    priceList: [],
    peoplelist:[],
    start: false,
    startClass: "",
    hiddenmodalput:true,
    modalStart: false,
    hiddmodal:false,
    inputWidth:150,
    promptText:"已收到通知，请到右下角【通知列表】里查看",
    publicStart:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 判断用户信息是否已经获取到
    var _this = this;
    if (app.globalData.userInfo && app.globalData.userInfo != '') {
      _this.setData({
        openid: app.globalData.userInfo.openid,
        barcode: options.id,
        startOpenid: options.openid,
      })
      _this.findData(options.id, options.openid);
    } else {
      wx.showLoading({
        title: '加载中...',
        mask:true,
      })
      // 获取用户信息
      app.getInfo(function (res) {
        if (res) {
          _this.setData({
            openid: res.openid,
            barcode: options.id,
            startOpenid: options.openid,
          })
          _this.findData(options.id, options.openid);
        } else {
          wx.hideLoading()
          // 没有授权，弹框授权
          _this.setData({
            modalStart: true,
            barcode: options.id,
            startOpenid: options.openid,
          })
        }
      })
    }
    _this.badd(options.id)
  },
  // 点击赞赏金额
  priceBtn:function(e){
    var price = e.currentTarget.dataset.price;
    this.fansData(price)
  },
  // 预留fromid
  // formSubmit:function(e){
  //   console.log(e)
  //   var fromid = e.detail.fromid;
  //   var price = e.detail.target.dataset.price;
  //   this.fansData(price,fromid)
  // },

  // 获取数据
  findData:function(res,openid){
    var _this = this;
    wx.showLoading({
      title: '加载中...',
      mask:true,
    })
    baseSet.select({ barcode: res, openid: openid }, function (res) {
      // console.log(res);
      wx.hideLoading()            
      if (res.data.code == 0) {
        var data = res.data.data
        


        _this.setData({
          datas: data.result[0],
          peoplelist:data.pay,
          priceList: util.objArray(data.result[0]),
          user:data.user_config,
          urlimg: data.headimgurl,
          nickname: data.nickname,
        })
        // 获取文字显示高度，判断是否隐藏
        if (data.result[0].status == 0) {
          var query = wx.createSelectorQuery();
          query.select('#text').boundingClientRect(function (rect) {
            if (rect.height >= "100") {
              _this.setData({
                start: true,
              })
            }
          }).exec();
        }
        
      }
    })
  },
  // 唤起支付
  fansData: function (price, isOther){
    console.log(this.data.barcode)
    var _this = this;
    wx.showLoading({
      title: '加载中...',
      mask: true,      
    })
    var isFind = false;
    var isPublic = true;
    var datas = { barcode: this.data.barcode, openid: this.data.openid, price: price ,isother:0};

    // 赞赏其他金额
    if (isOther == 1){
      datas.isother = 1;
    }

    // 获取是否有通知信息
    console.log(this.data.openid)
    baseSet.ifmassage({
      openid:this.data.startOpenid,
      wx_openid:this.data.openid,
      barcode: this.data.barcode,
      amount: price,
    },res=>{
      if(res.data.code==0){
        console.log(res)
        isFind = res.data.data.size;
        isPublic = res.data.data.isnot;
      }
    })

    // 获取支付信息
    baseSet.fansAdd(datas,res=>{
      wx.hideLoading()      
      if(res.data.code == 0){
        console.log(res.data.data)
        var data = res.data.data;
        // 唤起支付
        wx.requestPayment({
          timeStamp: data.timeStamp,
          nonceStr: data.nonceStr,
          package: data.package,
          signType: data.signType,
          paySign: data.paySign,
          success:res=>{
            console.log(res);
            if(res.errMsg == "requestPayment:ok"){

              console.log(isFind,isPublic)
              // 判断是否有通知信息，做弹框提示
              if(isFind){
                if (!isPublic){
                  _this.setData({
                    promptText: "已收到通知，请到右下角【通知列表】里查看",
                    hiddmodal:true, 
                    publicStart: false,                                    
                  })
                }else{
                  _this.setData({
                    promptText: "已收到通知，请到右下角【通知列表】里查看",
                    hiddmodal: true,
                    publicStart:true,
                  })
                }
                // wx.showModal({
                //   title: '提示',
                //   content: '已向你发送方案，请点击右下角的【收到通知】查阅方案',
                //   confirmText: "收到通知",
                //   success: res => {
                //     if (res.confirm) {
                //       wx.navigateTo({
                //         url: 'applist/applist?bid=' + this.data.barcode + "&openid=" + this.data.openid,
                //       })
                //       console.log('用户点击确定')
                //     } else if (res.cancel) {
                //       console.log('用户点击取消')
                //     }
                //   }
                // })
              }else{
                if (!isPublic){
                  _this.setData({
                    promptText: "已发送的通知将在小程序内展示可在右下角【通知列表】查看",
                    hiddmodal: true,
                    publicStart: false,                      
                  })
                }else{
                  wx.showToast({
                    title: '支付成功',
                  })
                }
              }
              _this.findData(_this.data.barcode, _this.data.startOpenid);              
            }else{
              wx.showToast({
                title: '支付失败',
              })
            }
          },
          fail:function(res){
            wx.showToast({
              title: '支付失败',
            })        
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },
  // 隐藏文字展开
  btnStart: function () {
    this.setData({
      start: false,
      startClass: "start",
    })
  },
  //取消按钮  
  cancel: function () {
    this.setData({
      hiddenmodalput: true,
      val: "",
    });
  },
  //确认  
  confirm: function () {
    this.fansData(this.data.val,1)    
    this.setData({
      hiddenmodalput: true,
      text: this.data.val,
      val: "",
    });

  },
  // 公众号取消
  cancelPublic:function(){
    this.setData({
      hiddmodal: false,
      publicStart: false,
    })
  },
  // 其他赞赏获取金额
  modalInput: function (e) {
    var val = e.detail.value
    var num = this.isInteger(val) ? val.length:val.length - 1;
    this.setData({ 
      val: val, 
      inputWidth: this.widthNun(num),
    });
  },
  //输入金额控制宽度
  isInteger:function(obj) {
    return obj % 1 === 0
  },
  widthNun: function (num) {
    var n = num * 50;
    if (n > 500) {
      return 500;
    }
    if (num < 3) {
      return 150;
    }
    return n;
  },
  // 弹出其他赞赏输入
  otherModel:function(){
    this.setData({
      hiddenmodalput: false,      
    })
  },
  // 其他赞赏聚焦
  focusBtn:function(){
    this.setData({
      focus:true,
    })
  },
  // 
  badd:function(bid){
    wx.showLoading({ title: "加载中..." ,mask:true})    
    baseSet.badd({barcode:bid},res=>{
      console.log(res)
      wx.hideLoading();
    })
  },
  // 直接到首页
  index:function(){
    wx.switchTab({
      url: '../index/index',
    })
  },
  // 到通知列表
  applist:function(){
    wx.navigateTo({
      url: 'applist/applist?bid=' + this.data.barcode + "&openid=" + this.data.openid,
    })
  },
  // 授权登录获取用户信息
  userInfo: function (e) {
    var _this = this;
    if (e.detail.errMsg == "getUserInfo:ok") {
      wx.showLoading({
        title: '加载中...',
        mask: true
      })
      _this.setData({
        modalStart: false,
      })
      // 获取用户信息
      app.getInfo(function (res) {
        if (res) {
          _this.setData({
            openid: res.openid
          })
          _this.findData(_this.data.barcode, _this.data.startOpenid);
        } else {
          wx.hideLoading()
          // 没有授权，弹框授权
          _this.setData({
            modalStart: true,
          })
        }
      })
    }
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // var query = wx.createSelectorQuery();
    // var _this = this;
    // query.select('.text-text1').boundingClientRect(function (rect) {
    //   console.log(rect)
    //   if (rect.height >= "100") {
    //     _this.setData({
    //       start: true,
    //     })
    //   }
    // }).exec();
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../img/share-index.png',
    }
  }
})