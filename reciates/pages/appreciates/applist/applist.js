const app = getApp()
const baseSet = app.baseSet().Interface;
const util = app.util();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取数据
    this.findPush(options.openid, options.bid);
    // 存储赞赏码id和openid
    this.setData({
      bid: options.bid,
      penid: options.openid,
    })
  },
  // 详情页带信息id
  notice: function (e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../../notice/notice?id=' + id,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  // 获取数据
  findPush: function (openid, bid) {
    baseSet.userIdPush({ openid: openid,barcode:bid}, res => {
      if (res.data.code == 0) {
        var data = res.data.data;
        for (var i = 0; i < data.length; i++) {
          for (var e = 0; e < data[i].da.length; e++) {
            data[i].da[e].time = util.formatTime(data[i].da[e].created_time * 1000, 'yyyy-MM-dd hh:mm', 1);
          }
        }
        this.setData({
          list: data
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../img/share-index.png',
    }
  }
})