const app = getApp()
const baseSet = app.baseSet().Interface;
const users = app.globalData;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    priceList: [],
    start:true,
    hiddenmodalput:true,
    text:"",
    val:"",
    sendText:"",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取金额
    wx.showLoading({
      title: '加载中',
      mask:true,
    })
    this.getPushServe(options.bid)
  },
  //聚焦
  focusBtn: function () {
    this.setData({
      focus: true,
    })
  },
  // 备注弹框
  addJia:function(){
    this.setData({
      hiddenmodalput:false,
      val:""
    })
  },
  // 备注弹框
  addJia1:function(){
    this.setData({
      hiddenmodalput: false,
      val:this.data.text
    })
  },
  //取消按钮  
  cancel: function () {
    this.setData({
      hiddenmodalput: true
    });
  },
  //确认  
  confirm: function () {

    this.setData({
      hiddenmodalput: true,
      text:this.data.val
    })
  },
  // 弹框输入内容
  modalInput:function(e){
    this.setData({
      val: e.detail.value
    })
  },
  // text输入内容
  textInput:function(e){
    this.setData({
      sendText: e.detail.value
    })
  },
  // 金额选择
  priceBtn(e){
    console.log(e)
    var index = e.currentTarget.dataset.index;
    var priceList = this.data.priceList;
    
    priceList[index].start = priceList[index].start?"":"start";

    this.setData({
      priceList: priceList
    })

  },
  // 重置
  reset:function(){
    var price = this.getPrice();
    if(price){
      wx.showModal({
        title: '提示',
        content: '重置后，之前赞赏的粉丝将无法收到后续通知，是否重置',
        success: res => {
          if (res.confirm) {
            wx.showLoading({
              title: '加载中',
              mask: true,
            })
            baseSet.reset({ openid: users.userInfo.openid, barcode: this.data.bid, price: price }, res => {
              console.log(res)
              if (res.data.code == 0) {
                wx.hideLoading()
                this.getPushServe(this.data.bid);
                wx.showToast({ title: '重置成功', })
              }
            })
            // console.log('用户点击确定')
          } else if (res.cancel) {
            // console.log('用户点击取消')
          }
        }
      })
    }else{
      wx.showModal({
        title: '提示',
        content: '选择金额后才能重置',
        showCancel:false,
      })     
    } 
  },
  // 发送
  save:function(){
    var price = this.getPrice(1);
    if (!this.data.sendText){
      wx.showToast({ title: '请输入通知内容', })
      return; 
    }
    if (!price){
      wx.showToast({ title: '请正确选择通知金额', })
      return;      
    }
    var data = {
      openid: users.userInfo.openid, barcode: this.data.bid, price: price, content: this.data.sendText
    }
    if(this.data.text){
      data.remark = this.data.text;
    }
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    baseSet.pushAdd(data, res => {
      // console.log(res)
      if(res.data.code == 0){
        wx.hideLoading()
        wx.showToast({ title: '通知发送成功', });
        // wx.navigateBack();
        this.setData({
          text: "",
          val: "",
          sendText: "",
        })
        this.getPushServe(this.data.bid);
        this.sendList();
      }
    })
  },
  // 查看列表
  sendList:function(){
    wx.navigateTo({
      url: '../sendlist/sendlist?bid='+this.data.bid,
    })
  },
  // 预览
  preview:function(){
    if(this.data.sendText){
      var datas = JSON.stringify(this.data)
      wx.navigateTo({
        url: 'preview/preview?data=' + datas,
      })
    }else{
      wx.showToast({
        title:"请输入通知内容"
      })
    }
    
  },
  // 便利数据
  getPrice:function(res){
    var str = "";
    var priceList = this.data.priceList;
    for (var i = 0; i < priceList.length; i++){
      if (priceList[i].start){
        if(res){
          if (priceList[i].num){
            return false;
          }
          str += priceList[i].amount + ","          
        }else{
          str += priceList[i].amount + ","
        }
      }
    }
    return str;
  },
  //获取数据 
  getPushServe:function(bid){
    console.log({ openid: users.userInfo.openid, barcode:bid})
    baseSet.getPushServe({ openid: users.userInfo.openid, barcode:bid},res=>{
      // console.log(res)
      if(res.data.code == 0){
        console.log(res.data)
        wx.hideLoading();
        var data = res.data.data.result;
        for(var i=0; i<data.length; i++){
          data[i].start = "";
          data[i].number = 3 - data[i].number;
          if(data[i].number == 0){
            data[i].num = "disabled"
          }else{
            data[i].num = ""
          }
        }
        this.setData({
          priceList:data,
          bid:res.data.data.appreciates.barcode,
        })
      }
    })
  },

  // 获取formid
  formSubmit: function (e) {
    // console.log(e)
    console.log(e.detail.formId)
    var fromId = e.detail.formId;
    if (fromId != "the formId is a mock one") {    
      baseSet.addform({
        openid: users.userInfo.openid,
        form_id: fromId
      }, res => {
        console.log(res);
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../img/share-index.png', 
    }
  }
})