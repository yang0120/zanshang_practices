const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    priceList: [
      { price: 0.8 },
      { price: 8 },
      { price: 18 },
      { price: 28 },
      { price: 58 },
      { price: 88 },
    ],
    start:false,
    startClass:"",
    text:"",
    datas:"",
    urlImg:"",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var datas = JSON.parse(options.data)
    this.setData({
      datas:datas,
      urlImg: app.globalData.userInfo.headimgurl,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var query = wx.createSelectorQuery();
    var _this = this;
    query.select('.text-text').boundingClientRect(function (rect) {
      if (rect.height >= "100"){
        _this.setData({
          start:true,
        })
      }
    }).exec();
  },
  // 展开
  btnStart:function(){
    this.setData({
      start: false,
      startClass:"start",
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../../img/share-index.png',
    }
  }
})