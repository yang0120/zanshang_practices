const app = getApp()
const baseSet = app.baseSet().Interface;
const util = app.util();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    priceList:[
      { price: 0.8, focus: false, num:60},
      { price: 8, focus: false, num:60},
      { price: 18, focus: false, num:60},
      { price: 28, focus: false, num:60},
      { price: 58, focus: false, num:60},
      { price: 88, focus: false, num:60},      
    ],
    publics:"",
    text:"",
    name: "",    
    startBtn:true,
    barcode:"",
    openid:"",

    inputStart:false,
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this = this;
  
    _this.setData({
      openid: app.globalData.userInfo.openid,
      inputStart:app.globalData.start,
    })
    // 判断是修改还是新建
    if (options.id) {
      _this.setData({
        startBtn: false,
        barcode: options.id,
      })
      _this.findData(options.id, app.globalData.userInfo.openid, )
    }
    
  },
  // 创建
  details:function(){
    var _this = this;
    wx.showLoading({
      title: '加载中...',
      mask:true,
    })
    _this.ifFn(function(){
      // 判断是否有重复金额
      if (util.repeat(_this.data.priceList)) {
        // 便利金额
        var datas = util.arrayObj(_this.data.priceList)
        if (datas) {
          datas.title = _this.data.name;
          datas.content = _this.data.text;
          datas.focus_no = _this.data.publics;
          datas.openid = _this.data.openid;
          baseSet.add(datas, function (res) {
            wx.hideLoading()
            console.log(res);
            wx.navigateTo({
              url: '../details/details?id=' + res.data.data.barcode,
            })
          })
        }else{
          wx.showToast({
            title: '请输入正确金额',
          })
        }
      } else {
        wx.showToast({
          title: '金额不能重复',
        })
      }
    })
  },
  // text输入聚焦
  focusBtn:function(){
    this.setData({
      focus:true,
    })
  },
  //金额输入聚焦
  focusInput:function(e){
    // console.log(e)

    var index = e.currentTarget.dataset.index;
    var priceList = this.data.priceList;
    for (var i = 0; i<priceList.length; i++){
      priceList[i].focus = false;
    }
    priceList[index].focus = true;
    this.setData({
      priceList: priceList,
    })
  },
  // 预览
  preview:function(){
    var _this = this;
    _this.ifFn(function(){
      var datas = JSON.stringify(_this.data);
      wx.navigateTo({
        url: 'preview/preview?data=' + datas,
      })
    })
    
  },
  // 返回
  cancel:function(){
    wx.navigateBack()                      
  },
  // 保存
  save:function(){
    var _this = this;
    _this.ifFn(function(){
      // 判断金额是否重复
      if (util.repeat(_this.data.priceList)){
        var datas = util.arrayObj(_this.data.priceList)
        if(datas){
          datas.title = _this.data.name;
          datas.content = _this.data.text;
          datas.focus_no = _this.data.publics;
          datas.barcode = _this.data.barcode;
          datas.openid = _this.data.openid;
          console.log(datas)
          wx.showLoading({
            title: '加载中...',
            mask: true,
          })
          baseSet.update(datas, function (res) {
            wx.hideLoading()
            if (res.data.code == 0) {
              wx.showToast({
                title: '保存成功',
              })
              wx.navigateBack()
            }
          })



          // 提示保存后将重置所有金额
        // wx.showModal({
        //   title: '提示',
        //   content: '保存后将重置该赞赏码,之前赞赏的粉丝将无法收到后续通知，是否保存',
        //   success: res => {
        //     if (res.confirm) {
        //       var datas = util.arrayObj(_this.data.priceList)              
        //       datas.title = _this.data.name;
        //       datas.content = _this.data.text;
        //       datas.focus_no = _this.data.publics;
        //       datas.barcode = _this.data.barcode;
        //       datas.openid = _this.data.openid;
        //       wx.showLoading({
        //         title: '加载中...',
        //         mask:true,
        //       })
        //       baseSet.update(datas, function (res) {
        //         wx.hideLoading()
        //         if (res.data.code == 0) {
        //           baseSet.reset({ openid: _this.data.openid, barcode: _this.data.barcode, price: _this.getPrice() }, res => {
        //             if (res.data.code == 0) {
        //               wx.navigateBack()                      
        //             }
        //           })
        //         }
        //       })
        //     } else if (res.cancel) {
        //     }
        //   }
        // })



        }else{
          wx.showToast({
            title: '请输入正确金额',
          })
        }
      }else{
        wx.showToast({
          title: '金额不能重复',
        })
      }
    })
  },
  // 名称
  nameInput:function(e){
    var val = e.detail.value;
    this.setData({
      name:val,
    })
  },
  // 引导语
  textInput:function(e){
    var val = e.detail.value;
    this.setData({
      text: val,
    })
  },
  // 公众号
  publicInput:function(e){
    var val = e.detail.value;
    this.setData({
      publics: val,
    })
  },
  
  // 添加
  addPriceList:function(){
    var priceList = this.data.priceList;
    for(var i=0; i<3; i++){
      priceList.push({ price: "", focus: false, num: 60});
    }
    this.setData({
      priceList: priceList
    })
  },
  // 删除
  removePriceList:function(){
    var priceList = this.data.priceList;
    priceList.splice(priceList.length - 3, 3)
    this.setData({
      priceList: priceList
    })
    
  },
  // 金额输入
  restaurInput:function(e){
    var index = e.currentTarget.dataset.index;
    var val = e.detail.value;
    var priceList = this.data.priceList;
    priceList[index].price = val;
    priceList[index].num = this.widthNun(val.length);
    this.setData({
      priceList: priceList
    })
  },
  // 计算宽
  widthNun:function(num){
    var n = num * 25;
    if(n > 100){
      return 100;
    }
    if(n == 0){
      return 25;      
    }
    return n;
  },
  // ios键盘的位置
  bindfocusBtn:function(e){
      var index = e.currentTarget.dataset.index;      
      wx.createSelectorQuery().select('#fill').boundingClientRect(function (rect) {
        // 使页面滚动到底部
        wx.pageScrollTo({
          scrollTop: rect.bottom
        })
      }).exec()
  },
  // 数据请求
  findData: function (res, openid) {
    wx.showLoading({title:"加载中...",mask:true})
    var _this = this;
    baseSet.select({ barcode: res, openid: openid }, function (res) {
      // console.log(res);
      wx.hideLoading()
      if (res.data.code == 0) {
        var data = res.data.data.result[0]
        var priceList = util.objArray(data);
        for (var i = 0; i < priceList.length; i++) {
          priceList[i].num = _this.widthNun(priceList[i].price.toString().length);
        }
        console.log(priceList)
        _this.setData({
          publics: data.focus_no,
          text: data.content,
          name: data.title,
          priceList: priceList,
        })
        

      }
    })
  },
  // 判断数据
  ifFn(callback){
    var data = this.data;
    if(!data.name){
      wx.showToast({
        title: "请输入赞赏名"
      })
      return false;
    }
    if(!data.text){
      wx.showToast({
        title: "请输入引导语"
      })
      return false;
    }
    // if (!data.text) {
    //   wx.showToast({
    //     title: "请输入引导语"
    //   })
    //   return false;
    // }

    for(var i=0; i<data.priceList.length; i++){
      if (!data.priceList[i].price || data.priceList[i].price == 0){
        wx.showToast({
          title: "请输入所有金额"
        })
        return false;
      }
    }
    callback();

  },
  // 重置
  getPrice: function () {
    var str = "";
    var priceList = this.data.priceList;
    for (var i = 0; i < priceList.length; i++) {
      str += priceList[i].price + ","      
    }
    return str;
  },
  // 获取formid
  formSubmit: function (e) {
    console.log(e.detail.formId)
    var fromId = e.detail.formId;
    if (fromId != "the formId is a mock one") {    
      baseSet.addform({
        openid: this.data.openid,
        form_id: fromId
      }, res => {
        console.log(res);
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../img/share-index.png', 
    }
  }
})