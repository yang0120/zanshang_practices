
const app = getApp()
const baseSet = app.baseSet().Interface;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    openid:"",
    start:false,
    datas:"",
    modalStart:false,
    author:false,
  },
  yan:function(){
    // wx.navigateTo({
    //   url: '../appreciates/appreciates?id=511536648660&openid=' + this.data.openid,
    // })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this = this;
    // 判断是否获取到用户信息
    if (app.globalData.userInfo && app.globalData.userInfo != '') {     
      _this.setData({
        openid: app.globalData.userInfo.openid,
        start: true
      })
      _this.selectData(app.globalData.userInfo.openid); 
    }else{
      wx.showLoading({
        title: '加载中...',
        mask:true,
      })
      wx.hideLoading()
      
      // 获取用户信息
      app.getInfo(function(res){
        if (res){
          _this.setData({
            openid: res.openid,
            start: true
          })
          _this.selectData(res.openid);
        }else{
          wx.hideLoading()
          // 没有授权，弹框授权
          _this.setData({
            modalStart:true,
            author:true,
          })
        }
      })
    }
  },
  // 添加赞赏码
  add:function(){
    wx.navigateTo({
      url: 'fill/fill',
    })
  },
  // 发送通知
  send:function(e){
    var id = e.currentTarget.dataset.id;        
    wx.navigateTo({
      url: 'send/send?bid='+id,
    })  
  },
  // 修改赞赏码
  modify:function(e){
    var id = e.currentTarget.dataset.id;    
    wx.navigateTo({
      url: 'fill/fill?id='+id,
    })
  },
  // 赞赏码详情
  details:function(e){
    console.log(e)
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: 'details/details?i=1&id='+id,
    })
  },
  // 获取数据
  selectData:function(res,i){
    var _this = this;
    baseSet.select({ openid:res},function(res){
      // console.log(res);
      if(!i){
        wx.hideLoading()        
      }
      if(res.data.code == 0){
        var data = res.data.data.result ? res.data.data.result : [];
        var num = 0;
        for(var i=0; i<data.length; i++){
          if (data[i].status == 0){
            num += 1;
          }
        }
        _this.setData({
          list: data,
          datas:res.data.data,
          startList:num,
        })
      }
    })
  },
  // 授权登录获取用户信息
  userInfo:function(e){
    var _this = this;
    if (e.detail.errMsg == "getUserInfo:ok"){
      wx.showLoading({
        title: '加载中...',
        mask:true,
      })
      _this.setData({
        modalStart: false,
      })
      app.getInfo(function (res) {
        if (res) {
          _this.setData({
            openid: res.openid,
            start: true,
            author:false,
          })
          _this.selectData(res.openid);
        } else {
          wx.hideLoading()
          _this.setData({
            modalStart: true,
          })
        }
      })
    }
  },
  // 删除
  remover:function(e){
    var id = e.currentTarget.dataset.id;    
    wx.showModal({
      title: '提示',
      content: '删除后该赞赏码的通知将无法查看，建议导出一份。是否确认删除',
      success: res=> {
        if (res.confirm) {

          baseSet.deleteAppreciates({barcode:id},res=>{
            if(res.data.code == 0){
              wx.showToast({
                title: '删除成功',
                duration: 2000,
                mask:true,
              })
              this.selectData(this.data.openid,1);       
              
            }
          })
          // console.log('用户点击确定')
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  // 获取formid
  formSubmit: function (e) {   
    var fromId = e.detail.formId;
    if (fromId != "the formId is a mock one"){
      baseSet.addform({
        openid: this.data.openid,
        form_id: fromId
      }, res => {
        console.log(res);
      })
    }
  },



  // share:function(){
  //   // var _this = this;
  //   // wx.showActionSheet({
  //   //   itemList: ['嵌入公众号', '生成分享图', '分享给好友/群'],
  //   //   success: function (res) {
  //   //     console.log(res.tapIndex)
  //   //     if (res.tapIndex == 0){
  //   //       _this.publics();
  //   //     } else if (res.tapIndex == 1){

  //   //     } else if (res.tapIndex == 2){
  //   //       // _this.onShareAppMessage()
  //   //     }
  //   //   },
  //   //   fail: function (res) {
  //   //     console.log(res.errMsg)
  //   //   }
  //   // })
  //   // return false;
  // },
  

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (this.data.author){
      if (app.globalData.userInfo && app.globalData.userInfo != '') {
        this.setData({
          openid: app.globalData.userInfo.openid,
          start: true,
          author:false,
          modalStart: false,
        })
        this.selectData(app.globalData.userInfo.openid);
      }
    }
    
    if(this.data.start){
      this.selectData(this.data.openid)
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) { 
      var that = this;
    　var shareObj = {
            title: app.globalData.title,   
      　　　　path: '/pages/index/index',  
              imageUrl: '../../img/share-index.png',  
      　　　　success: function (res) { 
            console.log(res)
        　　　　　　if (res.errMsg == 'shareAppMessage:ok') {
        　　　　　　}
      　　　　},
      　　　　fail: function (res) {
        　　　　　　if (res.errMsg == 'shareAppMessage:fail cancel') {

        　　　　　　} else if (res.errMsg == 'shareAppMessage:fail') {

        　　　　　　}
      　　　　}
  　　};
  　　if(options.from == 'button') {
      console.log(options.target.dataset.bid)
        shareObj.path = '/pages/appreciates/appreciates?id=' + options.target.dataset.bid + "&openid=" + this.data.openid;
      shareObj.title = "快来赞赏" + app.globalData.userInfo.nickname +"，获取方案吧！";
        shareObj.imageUrl = "../../img/share-user.png"
  　　}
　　  return shareObj;
  }
})
