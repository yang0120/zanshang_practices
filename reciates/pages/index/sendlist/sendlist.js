const app = getApp()
const baseSet = app.baseSet().Interface;
const util = app.util();
const users = app.globalData;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取数据
    this.findPush(options.bid)
    this.setData({
      bid: options.bid,
    })
  },
  // 历史页
  history:function(){
    wx.navigateTo({
      url: 'history/history?bid='+this.data.bid,
    })
  },
  // 详情页
  notice:function(e){
    var id = e.currentTarget.dataset.id;                
    wx.navigateTo({
      url: '../../notice/notice?id='+id,
    })
  },
  // 删除功能
  remove:function(e){
    var unid = e.currentTarget.dataset.unid;
    var _this = this;    
    wx.showModal({
      title: '提示',
      content: '是否确认删除',
      success: function (res) {
        if (res.confirm) {
          baseSet.deletes({ unid: unid }, res => {
            if (res.data.code == 0) {
              wx.showToast({ title: '删除成功', })
              _this.findPush(_this.data.bid)
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
   
  },
  // 获取数据接口
  findPush:function(bid){
    baseSet.findPush({ openid: users.userInfo.openid, barcode: bid, type: 0, status:0,},res=>{
      if(res.data.code == 0){
        var data = res.data.data;
        for (var i = 0; i < data.length; i++) {
          for (var e = 0; e < data[i].da.length; e++) {
            data[i].da[e].time = util.formatTime(data[i].da[e].created_time * 1000, 'yyyy-MM-dd hh:mm', 1);
          }
          data[i].da.sort(util.compare("created_time"))
        }
        
        this.setData({
          list:data
        })
      }
    })
  },
  
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      ttitle: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../img/share-index.png', 
    }
  }
})