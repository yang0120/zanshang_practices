const app = getApp()
const baseSet = app.baseSet().Interface;
const util = app.util();
const users = app.globalData;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    startDate: "2018-01-01",
    endDate: "2018-08-01",
    end: "",
    start: false,
    list:[],
    showStart:true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var date = util.formatTime(new Date().getTime(), "yyyy-MM-dd");
    var dates = util.formatTime(new Date().getTime(), "yyyy-MM");
    //获取默认时间
    this.setData({
      startDate: dates + "-01",
      endDate: date,
      end: date,
      bid: options.bid,
    })
    // 获取数据
    this.findPush({ openid: users.userInfo.openid, barcode: options.bid, type: 1, status: 0, })
  },
  //开始时间
  startChange: function (e) {
    // console.log(e)
    this.setData({
      startDate: e.detail.value,
      showStart:false,
    })
  },
  // 结束时间
  endChange: function (e) {
    // console.log(e)
    this.setData({
      endDate: e.detail.value,
      showStart:false,
    })
  },
  // 时间搜索是否隐藏
  showTime: function () {
    this.setData({
      start: !this.data.start,
    })
  },
  // 时间查询
  pickerBtn: function () {
    var data = {
      openid: users.userInfo.openid, 
      barcode: this.data.bid, 
      type: 1, 
      status: 0,
      start_time: util.timeFormat(this.data.startDate),
      end_time: util.timeFormat(this.data.endDate) + 32*60*60,
    }
    this.findPush(data);
  },
  // 详情
  notice: function (e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../../../notice/notice?id=' + id,
    })
  },
  // 导出，默认为获取的默认时间导出
  exports: function () {
    var text = "当前导出为" + this.data.startDate + "_" + this.data.endDate+"时间段的数据，是否确认导出";
    if (this.data.showStart) {
      text = '当前操作未进行时间筛选,请选择是否导出默认的当月数据'
    }
    wx.showModal({
      title: '提示',
      content: text,
      success: res => {
        if (res.confirm) {
          // console.log('用户点击确定')

          var datas = {
            openid: users.userInfo.openid,
            barcode: this.data.bid,
            type: 1,
            status: 1,
            start_time: util.timeFormat(this.data.startDate),
            end_time: util.timeFormat(this.data.endDate) + 32 * 60 * 60,
          }
          baseSet.findPush(datas, res => {
            if (res.data.code == 0) {
              var text = "方案记录_" + this.data.startDate + "_" + this.data.endDate;
              baseSet.downFike(res.data.data, text);
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  // 获取数据
  findPush: function (data) {
    wx.showLoading({ title: "加载中...",mask:true})                         
    baseSet.findPush(data, res => {
      wx.hideLoading();      
      if (res.data.code == 0) {
        var data = res.data.data;
        for (var i = 0; i < data.length; i++) {
          for (var e = 0; e < data[i].da.length; e++) {
            data[i].da[e].time = util.formatTime(data[i].da[e].created_time * 1000, 'yyyy-MM-dd hh:mm', 1);
          }
          data[i].da.sort(util.compare("created_time"))          
        }
        this.setData({
          list: data
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../../img/share-index.png', 
    }
  }
})