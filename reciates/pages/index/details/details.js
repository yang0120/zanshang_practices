const app = getApp()
const baseSet = app.baseSet().Interface;
const util = app.util();
const users = app.globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    priceList:[],
    start:true,
    datas:{},
    urlCode:"",
    hiddenmodalput:true,
    val:"",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this = this;
    if (options.i){
      this.setData({
        start:false,
      })
    }
    _this.setData({
      openid: users.userInfo.openid,
      id: options.id,
    })
    // 获取数据
    _this.findData(options.id, users.userInfo.openid);   
  },
  //公众号页
  publics:function(){
    wx.navigateTo({
      url: '../public/public?bid=' + this.data.bid,
    })
  },
  // 收支记录
  refund:function(){
    wx.navigateTo({
      url: 'refund/refund?bid='+this.data.bid,
    })
  },
  // 金额详情
  priceFans:function(e){
    var price = e.currentTarget.dataset.price;
    wx.navigateTo({
      url: 'fans/fans?bid='+this.data.bid+"&price="+price,
    })
  },
  // 下载二维码
  saveImg:function(){
    this.setData({
      hiddenmodalput:false
    })
    // baseSet.downImg(this.data.urlCode, res => {
    //   console.log(res)
    //   wx.hideLoading();
    // })
  },
  // 确定
  confirm:function(){
    if(this.data.val){
      this.getImg();
      this.setData({
        hiddenmodalput: true,
      })
    }else{
      wx.showToast({
        title: '请输入提示语',
      })
    }
    
  },
  // 取消
  cancel:function(){
    this.setData({
      hiddenmodalput: true,
      val:'',
    })
  },
  // 弹框数据
  modalInput:function(e){
    console.log(e);
    this.setData({
      val: e.detail.value,
    })
  },
  // 画出二维码
  getImg:function(){
    wx.showLoading({
      title: '图片生成中...',
      mask: true,
    })
    var _this = this;
    // 绘制前把图片下载下来
    wx.downloadFile({
      url: users.userInfo.headimgurl,
      success:res=>{
        var head = res.tempFilePath;
        // 绘制圆形头像
        var contex = wx.createCanvasContext("avatarCanvas");
        contex.save();
        contex.beginPath();
        contex.arc(300 / 2, 300 / 2, 300 / 2, 0, Math.PI * 2, false);
        contex.clip();
        contex.drawImage(head, 0, 0, 300, 300);
        contex.restore();
        contex.draw(true, function () {
          // 头像绘制完成后下载图片
          wx.canvasToTempFilePath({
            x: 0,
            y: 0,
            width: 300,
            height: 300,
            destWidth: 800,
            destHeight: 800,
            canvasId: 'avatarCanvas',
            success: res => {
              console.log(res)

              var avatar = res.tempFilePath;
              // 下载二维码
              wx.downloadFile({
                url: _this.data.urlCode,
                success: res => {
                  var code = res.tempFilePath;
                  // 绘制效果图
                  const ctx = wx.createCanvasContext('imgCanvas')
                  // 绘制背景图
                  ctx.drawImage("../../../img/bg.png", 0, 0, 300, 300);
                  // 绘制二维码
                  ctx.drawImage(code, 85, 30, 130, 130);
                  // 绘制头像
                  ctx.drawImage(avatar, 121, 66, 58, 58);
                  // 设置名称字体
                  ctx.setTextAlign('center')
                  ctx.setFontSize(14);
                  ctx.setFillStyle("#999999");
                  // 绘制名称
                  ctx.fillText(users.userInfo.nickname, 150, 182)
                  // 设置提示字体
                  ctx.setFillStyle("#000000");
                  // 绘制提示语
                  ctx.fillText(_this.data.val, 150, 206)
                  ctx.draw(true, function () {
                    // 绘制完成下载图片
                    wx.canvasToTempFilePath({
                      x: 0,
                      y: 0,
                      width: 300,
                      height: 300,
                      destWidth: 3200,
                      destHeight: 3200,
                      canvasId: 'imgCanvas',
                      success: res => {
                        // 保存图片
                        wx.saveImageToPhotosAlbum({
                          filePath: res.tempFilePath,
                          success: res => {
                            console.log(res)
                            wx.hideLoading();
                            _this.setData({
                              val: ""
                            })
                            wx.showToast({
                              title: "保存成功"
                            })
                          },
                          fail: res => {
                            wx.hideLoading();
                            wx.showToast({
                              title: "保存失败"
                            })
                          }
                        })
                      },
                      fail:res=>{
                        wx.hideLoading();
                        wx.showToast({
                          title: "下载失败"
                        })
                      }
                    })
                  })
                },
                fail:res=>{
                  wx.hideLoading();
                  wx.showToast({
                    title: "下载失败"
                  })
                }
              })
              
            },
            fail:res=>{
              wx.hideLoading();
              wx.showToast({
                title: "下载失败"
              })
            }
          })
        }) 
      },
      fail:res=>{
        wx.hideLoading();
        wx.showToast({
          title: "下载失败"
        })
      }
    })
    
  },


  // 获取数据
  findData:function(res,openid){
    wx.showLoading({
      title: '加载中...',
      mask:true,
    })
    var _this = this;
    baseSet.select({barcode:res,openid:openid},function(res){
      // console.log(res);
      wx.hideLoading()
      if(res.data.code == 0){
        var data = res.data.data
        _this.setData({
          datas:data,
          bid: data.result[0].barcode,
          urlCode: data.result[0].url_code,
          title: data.result[0].title,
          priceList:util.objArray(data.result[0])
        })

      }
    })
  },
  // 获取formid
  formSubmit: function (e) {
    var fromId = e.detail.formId;
    if (fromId != "the formId is a mock one") {
      baseSet.addform({
        openid: this.data.openid,
        form_id: fromId
      }, res => {
        // console.log(res);
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // console.log(this.data.aa)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    // 返回index页
    if(this.data.start){
      wx.navigateBack({ dalta: 6 })      
    }
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) {
    var that = this;
    var shareObj = {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../img/share-index.png',
      success: function (res) {
        if (res.errMsg == 'shareAppMessage:ok') {
        }
      },
      fail: function (res) {
        if (res.errMsg == 'shareAppMessage:fail cancel') {

        } else if (res.errMsg == 'shareAppMessage:fail') {

        }
      }
    };
    if (options.from == 'button') {
      shareObj.path = '/pages/appreciates/appreciates?id=' + this.data.bid + "&openid=" + this.data.openid;
      shareObj.title = "快来赞赏" + app.globalData.userInfo.nickname + "，获取方案吧！";
      shareObj.imageUrl = "../../../img/share-user.png"
    }
    return shareObj;
  }
})