const app = getApp()
const baseSet = app.baseSet().Interface;
const util = app.util();
const users = app.globalData;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    start:"start",
    tab:"",
    tabs1list:[],
    tabs2list:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取数据
    this.fans(users.userInfo.openid, options.bid, options.price);
    this.findPush(options.bid, options.price)
    // 赞赏码id和金额
    this.setData({
      price: options.price,
      bid: options.bid,
    })
  },
  // 获取粉丝数据
  fans: function (openid, bid, price){
    wx.showLoading({ title: "加载中..." ,mask:true})    
    var _this = this;
    baseSet.getFans({
      openid: openid,
      barcode: bid,
      amount:price
    },res=>{
      wx.hideLoading()
      // console.log(res)
      if(res.data.code == 0){
        var data = res.data.data;
        for(var i = 0; i<data.length; i++){
          for(var e = 0; e<data[i].da.length; e++){
            data[i].da[e].time = util.formatTime(data[i].da[e].pay_time * 1000, 'yyyy-MM-dd hh:mm',1);
          }
        }
        console.log(data)
        _this.setData({
          tabs1list:data,
        })
      }
    })
  },
  //获取通知数据
  findPush: function (bid, amount) {
    baseSet.findPush({ openid: users.userInfo.openid, barcode: bid, type: 1, status: 0, amount: amount }, res => {
      if (res.data.code == 0) {
        var data = res.data.data;
        for (var i = 0; i < data.length; i++) {
          for (var e = 0; e < data[i].da.length; e++) {
            data[i].da[e].time = util.formatTime(data[i].da[e].created_time * 1000, 'yyyy-MM-dd hh:mm', 1);
          }
          data[i].da.sort(util.compare("created_time"))          
        }
        this.setData({
          tabs2list: data
        })
      }
    })
  },
  // tab
  tabBtn1:function(){
    this.setData({
      start:"start",
      tab:"",
    })
  },
  tabBtn2: function () {
    this.setData({
      start: "",
      tab: "start",
    })
  },
  // 删除
  // remove: function (e) {
  //   var _this =this;
  //   wx.showModal({
  //     title: '提示',
  //     content: '是否确认删除',
  //     success: function (res) {
  //       if (res.confirm) {
  //         var unid = e.currentTarget.dataset.unid;
  //         baseSet.deletes({ unid: unid }, res => {
  //           if (res.data.code == 0) {
  //             wx.showToast({ title: '删除成功', })
  //             _this.findPush(_this.data.bid,_this.data.price)
  //           }
  //         })
  //       } else if (res.cancel) {
  //         console.log('用户点击取消')
  //       }
  //     }
  //   })
    
  // },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../../img/share-index.png',
    }
  }
})