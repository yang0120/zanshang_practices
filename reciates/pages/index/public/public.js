const app = getApp()
const baseSet = app.baseSet().Interface;
const users = app.globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    url:"地址",
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取数据
    this.setData({
      url: "pages/appreciates/appreciates?id=" + options.bid +"&openid="+ users.userInfo.openid
    })
  },
  // 复制
  copyTBL:function(){
    var _this = this;
    wx.setClipboardData({
      data: _this.data.url,
      success:function(){
        wx.showToast({
          title: '复制成功',
          icon: 'success',
          duration: 2000
        })
      },
      fail:function(){
        wx.showToast({
          title: '复制失败',
          duration: 2000
        })
      }
    })
  } , 
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../img/share-index.png', 
    }
  }
})