const app = getApp()
const baseSet = app.baseSet().Interface;
const users = app.globalData;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: "",
    config:"",
    price:0,
    modalStart:false,
    author:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this =this;
    if (users.userInfo){
      this.setData({
        user: users.userInfo
      })
      wx.showLoading({ title: "加载中...", mask: true })
      this.getPrice();
    }else{
      wx.showLoading({ title: "加载中...", mask: true })      
      app.getInfo(function (res) {
        if (res) {
          this.setData({
            user: res,
          })
          this.getPrice();
        } else {
          wx.hideLoading()
          // 没有授权，弹框授权
          _this.setData({
            modalStart: true,
            author:true,
          })
        }
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },
  // 钱包
  walletBtn:function(){
    wx.navigateTo({
      url: 'wallet/wallet',
    })
  },
  // 常见问题
  problemBtn:function(){
    wx.navigateTo({
      url: 'problem/problem',
    })
  },
  // 下载列表
  downBtn: function () {
    wx.navigateTo({
      url: 'down/down',
    })
  },
  // 更多设置
  setup:function(){
    wx.navigateTo({
      url: 'setup/setup',
    })
  },
  // 银行卡
  cardBtn:function(){
    wx.navigateTo({
      url: 'bank/bank',
    })
  },
  // 客服
  customerBtn:function(){
    wx.navigateTo({
      url: 'customer/customer',
    })
  },
  // 钱包价格
  getPrice:function(){
    baseSet.getPrice({ openid: users.userInfo.openid},res=>{
      // console.log(res)
      wx.hideLoading();
      this.setData({
        price:res.data.data.price,
      })
    })
  },
  // 授权登录获取用户信息
  userInfo: function (e) {
    var _this = this;
    if (e.detail.errMsg == "getUserInfo:ok") {
      wx.showLoading({
        title: '加载中...',
        mask: true,
      })
      _this.setData({
        modalStart: false,
      })
      app.getInfo(function (res) {
        if (res) {
          _this.setData({
            user: res,
            author:false,
          })
          _this.getPrice();
        } else {
          wx.hideLoading()
          // 没有授权，弹框授权
          _this.setData({
            modalStart: true,
          })
        }
      })
    }
  },
  contactBtn:function(e){
    console.log(e)
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (this.data.author) {
      if (users.userInfo) {
        this.setData({
          user: users.userInfo,
          author:false,
          modalStart: false,          
        })
        wx.showLoading({ title: "加载中...", mask: true })
        this.getPrice();
      }
    }

    if (users.userInfo){
      this.getPrice();           
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../img/share-index.png',
    }
  }
})