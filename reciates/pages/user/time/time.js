const app = getApp()
const baseSet = app.baseSet().Interface;
const users = app.globalData;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    time:"12:00",
    cheack:"",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getConfig();
  },
  changeTime: function (e) {
    if(this.data.cheack){
      this.setData({
        time: e.detail.value
      })
      this.updateConfig({ openid: users.userInfo.openid, update_time: e.detail.value })
    }else{
      wx.showModal({
        title: '提示',
        content: '自动更新时间未开启，请开启后再设置时间',
        showCancel:false,
      })
    }
    
  },
  switchChange: function (e) {
    this.setData({ cheack: e.detail.value })
    this.updateConfig({openid: users.userInfo.openid, is_update: e.detail.value ? 1 : 0 })
  },
  getConfig: function () {
    wx.showLoading({ title: "加载中", mask: true })    
    baseSet.getConfig({ openid: users.userInfo.openid }, res => {
      wx.hideLoading()
      console.log(res)
      if (res.data.code == 0) {
        this.setData({
          cheack: res.data.data.is_update == 1 ? true : false,
          time: res.data.data.update_time,
        })
      }
    })
  },
  updateConfig: function (data) {
    wx.showLoading({ title: "加载中...", mask: true })        
    baseSet.updateConf(data, res => {
      wx.hideLoading()
      console.log(res)
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../img/share-index.png',
    }
  }
})