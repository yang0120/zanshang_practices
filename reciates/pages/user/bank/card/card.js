const app = getApp()
const baseSet = app.baseSet().Interface;
const users = app.globalData;
const bank = require("../../../../utils/bank.js");

const reg = new RegExp("^([1-9]{1})(\\d{14}|\\d{18})$");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name:"",
    trueName:"",
    card:"",
    bank:"",
    typeCard:"",
    account:"",
    start:true,
    cardList:["请选择银行","中国银行","北京银行","民生银行","广发银行","邮政银行","工商银行","农业银行","建设银行"],
    cardNum:0,
    copy:"zanzs2101",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.i){
      this.getBank();
      this.setData({
        start:false,
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },
  // 名称
  nameInput:function(e){
    var val = e.detail.value;
    this.setData({
      name:val,
    })    
  },
  // 真实姓名
  trueNameInput:function(e){
    var val = e.detail.value;
    this.setData({
      trueName: val,
    })
  },
  //银行卡号
  cardInput:function(e){
    var val = e.detail.value;
    this.setData({
      card: val,
    })
  },
  // 银行类型
  typeCardInput:function(e){
    var val = e.detail.value;
    this.setData({
      typeCard: val,
    })
  },
  // 开户行
  accountInput:function(e){
    var val = e.detail.value;
    this.setData({
      account: val,
    })
  },
  // 银行
  bankInput: function (e) {
    var val = e.detail.value;
    this.setData({
      bank: val,
    })
  },
  // 银行list
  cardListBtn:function(e){
    var num = e.detail.value;
    var cardList = this.data.cardList;
    this.setData({
      cardNum:num,
      bank: num?cardList[num]:"",
    })
  },
  // 保存
  cardBtn:function(){
    this.addCard();
  },
  // 修改
  updateBtn:function(){
    this.update();
  },
  // 复制
  copyBtn:function(){
    wx.setClipboardData({
      data: this.data.copy,
      success: function () {
        wx.showToast({
          title: '复制成功',
          icon: 'success',
          duration: 2000
        })
      },
      fail: function () {
        wx.showToast({
          title: '复制失败',
          duration: 2000
        })
      }
    })
  },
  // 修改数据
  update:function(){
    var data = this.data;
    if (this.cardToast()) {
      wx.showLoading({
        title: '加载中...',
        mask: true,
      })
      baseSet.bankUpdate({
        openid: users.userInfo.openid,
        nickname: data.name,
        name: data.trueName,
        bank_number: data.card,
        // bank_status: data.typeCard,
        bank_account: data.account,
        bank_id:data.id,
        yh: data.bank,
      }, res => {
        wx.hideLoading();              
        console.log(res);
        if (res.data.code == 0) {
          wx.showToast({
            title: '修改成功',
            mask:true,
          })
          setTimeout(()=>{
            wx.navigateBack();            
          },1500)
        }
      })
    }
  },
  // 添加数据
  addCard:function(){
    var data = this.data;
    console.log(data.trueName)
    if (this.cardToast()){
      wx.showLoading({
        title: '加载中...',
        mask: true,
      })
      baseSet.bankAdd({
        openid:users.userInfo.openid,
        nickname:data.name,
        name:data.trueName,
        bank_number:data.card,
        // bank_status:data.typeCard,
        bank_account:data.account,
        yh:data.bank
      },res=>{
        wx.hideLoading();              
        console.log(res);
        if(res.data.code == 0){
          wx.showToast({
            title: '添加成功',
          })
          setTimeout(() => {
            wx.navigateBack();
          }, 1500)
        }
      })
    }

  },
  // 自动识别银行
  blurBtn: function (e) {
    var val = e.detail.value;
    var data = bank.bankCardAttribution(val)
    console.log(data)
    if (data != "error") {
      this.setData({
        bank: data.bankName,
        typeCard: data.cardTypeName
      })
    }else{
      wx.showToast({
        title: '请输入正确卡号',
      })
      this.setData({
        bank: "",
        typeCard: "",
      })
    }
  },

  // 判断数据
  cardToast:function(){
    var data = this.data;
    // if(!data.name){
    //   wx.showToast({
    //     title: '请输入姓名',
    //   })
    //   return false;
    // }
    if (!data.trueName) {
      wx.showToast({
        title: '请输入真实姓名',
      })
      return false;
    }

    // 选择银行
    if (!data.card) {
      wx.showToast({
        title: '请输入银行卡号',
      })
      return false;
    } else if (!reg.exec(data.card)){
      wx.showToast({
        title: '请输入正确卡号',
      })
      return false;
    }

    // 自动识别银行
    // if (!data.card) {
    //   wx.showToast({
    //     title: '请输入银行卡号',
    //   })
    //   return false;
    // } else if (bank.bankCardAttribution(data.card) == "error") {
    //   wx.showToast({
    //     title: '请输入正确卡号',
    //   })
    //   return false;
    // }

    // 自动识别银行注释，选择银行开启

    if (!data.bank) {
      wx.showToast({
        title: '请输入银行',
      })
      return false;
    }
    // if (!data.typeCard) {
    //   wx.showToast({
    //     title: '请输入银行卡类型',
    //   })
    //   return false;
    // }


    if (!data.account) {
      wx.showToast({
        title: '请输入开户行',
      })
      return false;
    }
    return true;
  },

  // 获取数据
  getBank: function () {
    wx.showLoading({
      title: '加载中...',
      mask: true,
    })
    baseSet.bankFind({ openid: users.userInfo.openid }, res => {
      console.log(res)
      wx.hideLoading();      
      if (res.data.code == 0) {
        var data = res.data.data;
        var cardList = this.data.cardList;
        var num = 0;
        for(var i = 0; i < cardList.length; i++){
          if (cardList[i] == data.yh){
            num = i;
          }
        }

        this.setData({
          name: data.nickname,
          trueName: data.name,
          card: data.bank_number,
          typeCard: data.bank_status,
          account: data.bank_account,
          id: data.bank_id,
          bank:data.yh,
          cardNum:num,
        })
      }
    })

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../../img/share-index.png',
    }
  }
})