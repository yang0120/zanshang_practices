const app = getApp()
const baseSet = app.baseSet().Interface;
const users = app.globalData;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    start:false,
    data:{},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  // 查看银行卡
  lookBank:function(){
    wx.navigateTo({
      url: 'card/card?i=1',
    })
  },
  // 添加银行卡
  addBank:function(){
    wx:wx.navigateTo({
      url: 'card/card',
    })
  },
  // 获取数据
  getBank:function(){
    wx.showLoading({
      title: '加载中...',
      mask:true,
    })
    baseSet.bankFind({openid:users.userInfo.openid},res=>{
      // console.log(res)
      wx.hideLoading();
      if(res.data.code == 0){

        var data = res.data.data;

        if (JSON.stringify(data) != "{}"){
          data.card = data.bank_number.substr(data.bank_number.length - 4)           
        }
        this.setData({
          data:data,
        })
      }
    })

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getBank();    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../img/share-index.png',
    }
  }
})