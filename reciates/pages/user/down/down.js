const app = getApp()
const baseSet = app.baseSet().Interface;
const util = app.util();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getData();
  },
  // 获取数据
  getData:function(){
    // 获取下载列表
    wx.getSavedFileList({
      success: res => {
        console.log(res)
        var arr = res.fileList;
        // 获取storage
        wx.getStorage({
          key: 'data',
          success: res => {
            if (res.data) {
              var data = JSON.parse(res.data);
              console.log(data)
              // 通过文件名下载列表与storage对比获取title
              for (var i = 0; i < arr.length; i++) {
                arr[i].time = util.formatTime(new Date(arr[i].createTime * 1000), 'yyyy-MM-dd hh:mm:ss');
                arr[i].file = this.splits(arr[i].filePath);
                for (var e = 0; e < data.length; e++) {
                  if (data[e].url == arr[i].filePath) {
                    arr[i].title = data[e].title;
                  }
                }
              }
              // arr.reverse()
              arr.sort(util.compare("createTime"));
              console.log(arr)
              this.setData({
                list: arr
              })
            }
          },
          fail: res => {
            for (var i = 0; i < arr.length; i++) {
              arr[i].time = util.formatTime(new Date(arr[i].createTime * 1000), 'yyyy-MM-dd hh:mm:ss');
              arr[i].file = this.splits(arr[i].filePath);
              arr[i].title = "";
            }
            this.setData({
              list: arr
            })
          }
        })
      }
    })
  },

 


  splits:function(str){
    // var str = str.split("http://store/");
    var str = str.split("//");
    str = str[1].split(".xlsx");

    return str[0];
  },
  itemBtn:function(e){
    var id = e.currentTarget.dataset.id;
    wx.openDocument({
      filePath:id,
      success:res=>{
        // console.log()
      }
    })
  },
  copy: function (e) {
    var _this = this;
    var id = e.currentTarget.dataset.id;
    // wx.removeSavedFile({
    //   filePath:id,
    //   success: function () {
    //     wx.showToast({
    //       title: '删除成功',
    //       icon: 'success',
    //       duration: 2000
    //     })
    //     _this.getData();
        
    //   },
    //   fail: function () {
    //     wx.showToast({
    //       title: '删除失败',
    //       duration: 2000
    //     })
    //   }
    // })
    wx.setClipboardData({
      data: id,
      success: function () {
        wx.showToast({
          title: '复制成功',
          icon: 'success',
          duration: 2000
        })
      },
      fail: function () {
        wx.showToast({
          title: '复制失败',
          duration: 2000
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../img/share-index.png',
    }
  }
})