const app = getApp()
const baseSet = app.baseSet().Interface;
const users = app.globalData;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    radio1:"start",
    radio2:"",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getConfig();    
  },
  radio1Btn:function(){
    this.setData({
      radio1: "start",
      radio2: "",
    })
    this.updateConfig({ openid: users.userInfo.openid, is_notice: 1 })    
  },
  radio2Btn: function () {
    this.setData({
      radio1: "",
      radio2: "start",
    })
    this.updateConfig({ openid: users.userInfo.openid, is_notice: 0})        
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },
  getConfig: function () {
    wx.showLoading({ title: "加载中", mask: true })    
    baseSet.getConfig({ openid: users.userInfo.openid }, res => {
      wx.hideLoading()
      console.log(res)
      if (res.data.code == 0) {
        this.setData({
          radio1: res.data.data.is_notice== 1?"start":"",
          radio2: res.data.data.is_notice == 0 ? "start" : "",
        })
      }
    })
  },
  updateConfig: function (data) {
    wx.showLoading({ title: "加载中...", mask: true })        
    baseSet.updateConf(data, res => {
      wx.hideLoading()
      console.log(res)
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../img/share-index.png',
    }
  }
})