const app = getApp()
const baseSet = app.baseSet().Interface;
const users = app.globalData;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    data:{},
    price:"",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getBank();
    this.getPrice();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },
  praceInput:function(e){
    var val = e.detail.value;
    this.setData({
      value:val,
    })    
  },
  // 提现
  rawalBtn:function(){
    if(this.data.value){
      if (((this.data.value * 100).toString()).indexOf(".") != -1) {
        wx.showToast({
          title: '请正确输入金额',
          mask: true,
        })
        return false;
      } else if (this.data.value < 10){
        wx.showModal({
          title: '提示',
          content: '当前提现金额低于10元，请重新输入',
          showCancel:false,
        })
        return false;
      }

      wx.showModal({
        title: '提示',
        content: '提现需要1-2天的时间，请耐心等待',
        success: res=> {
          if (res.confirm) {
            // console.log('用户点击确定')
            wx.showLoading({
              title: '加载中...',
              mask: true,
            })
            var num = parseFloat(this.data.value)
            baseSet.getBank({
              bank_id: this.data.data.bank_id,
              amount: num,
              openid: users.userInfo.openid
            }, res => {
              wx.hideLoading();
              if (res.data.code == 0) {
                wx.showToast({
                  title: '提现成功',
                  mask: true,
                })
                setTimeout(() => {
                  wx.navigateBack();
                }, 1500)
              } else {
                wx.showToast({
                  title: res.data.msg.stateCode,
                  mask: true,
                })
              }
            })

          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })

      
    }else{
      wx.showToast({
        title: '请输入提现金额',
      })
    }
    
  },

  getPrice: function () {
    baseSet.getPrice({ openid: users.userInfo.openid }, res => {
      // console.log(res)
      if(res.data.code == 0){
        var data = res.data.data;
        
        this.setData({
          price: (data.price - data.frost_amount).toFixed(2),
          amount: data.frost_amount,
      })
      }
      
    })
  },


  getBank: function () {
    baseSet.bankFind({ openid: users.userInfo.openid }, res => {
      console.log(res)
      if (res.data.code == 0) {
        var data = res.data.data;
        data.card = data.bank_number.substr(data.bank_number.length - 4)
        this.setData({
          data: res.data.data,
        })
      }
    })

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../../img/share-index.png',
    }
  }
})