const app = getApp()
const baseSet = app.baseSet().Interface;
const util = app.util();
const users = app.globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    hiddenmodalput:true,
    val:"",
    name:"名称",
    price:0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      openid: users.userInfo.openid,
      userid: options.id
    })
    this.getpayment({ openid: users.userInfo.openid, type: 0, status: 0, user_id: options.id})
  },
  // 弹出修改备注
  showModal:function(){
    this.setData({
      hiddenmodalput:false,
    })
  },
  //取消按钮  
  cancel: function () {
    this.setData({
      hiddenmodalput: true,
      val: this.data.user.name ? this.data.user.name:this.data.user.nickname,
    });
  },
  //确认  
  confirm: function () {
    var val = this.data.val;
    wx.showLoading({ title: "加载中..." ,mask:true})             
    baseSet.updateName({
      openid :this.data.openid,
      user_id: this.data.user.user_id,
      nickname:val,
    },res=>{
      wx.hideLoading();      
      if(res.data.code == 0){
        var user = this.data.user;
        user.name = val;
        this.setData({
          hiddenmodalput: true,
          user: user,
          // val: "",
        })
      }
    })

    
  },
  // 备注数据
  modalInput: function (e) {
    this.setData({ val: e.detail.value });
  },
  // 获取数据
  getpayment: function (data) {
    wx.showLoading({ title: "加载中...",mask:true})        
    baseSet.getpayment(data, res => {
      wx.hideLoading();      
      if (res.data.code == 0) {
        var data = res.data.data.result;
        for (var i = 0; i < data.length; i++) {
          for (var e = 0; e < data[i].da.length; e++) {
            data[i].da[e].time = util.formatTime(data[i].da[e].pay_time * 1000, 'yyyy-MM-dd hh:mm',1);
          }
          // data[i].da.sort(util.compare("pay_time"));          
        }
        this.setData({
          list: data,
          user: data[0].da[0],
          val: data[0].da[0].name ? data[0].da[0].name : data[0].da[0].nickname,
          price: res.data.data.zongjia,
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../../img/share-index.png',
    }
  }
})