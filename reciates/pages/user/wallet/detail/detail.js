const app = getApp()
const baseSet = app.baseSet().Interface;
const util = app.util();
const users = app.globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    startDate: "2018-01-01",
    endDate: "2018-08-01",
    end: "",
    start: false,
    showStart: true,    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var date = util.formatTime(new Date().getTime(), "yyyy-MM-dd");
    var dates = util.formatTime(new Date().getTime(), "yyyy-MM");
    this.setData({
      startDate: dates + "-01",
      endDate: date,
      end: date,
      openid: users.userInfo.openid,
    });
    wx.showLoading({ title: "加载中...",mask:true })    
    this.getpayment({openid:users.userInfo.openid,type:0,status:0})
  },
  // 开始时间
  startChange: function (e) {
    // console.log(e)
    this.setData({
      startDate: e.detail.value,
      showStart:false,
    })
  },
  // 结束时间
  endChange: function (e) {
    // console.log(e)
    this.setData({
      endDate: e.detail.value,
      showStart: false,      
    })
  },
  // 隐藏显示时间搜索
  showTime: function () {
    this.setData({
      start: !this.data.start,
    })
  },
  // 详情
  deal:function(e){
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../deal/deal?id='+id,
    })
  },
  // 搜索
  pickerBtn:function(){
    var data = {
      type:1,
      startTime: util.timeFormat(this.data.startDate),
      endTime: util.timeFormat(this.data.endDate) + 32* 60 * 60,
      openid:this.data.openid,
      status:0,
    }
    wx.showLoading({ title: "加载中..." ,mask:true,})        
    this.getpayment(data);
  },
  // 导出
  exports:function(){
    var text = "当前导出为" + this.data.startDate + "_" + this.data.endDate + "时间段的数据，是否确认导出";
    
    if(this.data.showStart){
      text = '当前操作未进行时间筛选,请选择是否导出默认的当月数据'
    }
    wx.showModal({
      title: '提示',
      content: text,
      success: res => {
        if (res.confirm) {
          var data = {
            type: 1,
            startTime: util.timeFormat(this.data.startDate),
            endTime: util.timeFormat(this.data.endDate) + 32 * 60 * 60,
            openid: this.data.openid,
            status: 1,
          }
          wx.showLoading({ title: "加载中...", mask: true })
          baseSet.getpayment(data, res => {
            wx.hideLoading()
            if (res.data.code == 0) {
              var text = "收支记录_" + this.data.startDate + "_" + this.data.endDate;
              baseSet.downFike(res.data.data, text);
            }
          })

        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  // 获取数据
  getpayment:function(data){
    baseSet.getpayment(data,res=>{
      wx.hideLoading()      
      if(res.data.code == 0){
        var data = res.data.data.result;
        for (var i = 0; i < data.length; i++) {
          for (var e = 0; e < data[i].da.length; e++) {
            data[i].da[e].time = util.formatTime(data[i].da[e].pay_time * 1000, 'yyyy-MM-dd hh:mm',1);
          }
          // data[i].da.sort(util.compare("pay_time"));
        }
        this.setData({
          list:data,
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getpayment({ openid: users.userInfo.openid, type: 0, status: 0 })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../../img/share-index.png',
    }
  }
})