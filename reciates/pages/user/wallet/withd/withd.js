const app = getApp()
const baseSet = app.baseSet().Interface;
const util = app.util();
const users = app.globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    end: "",
    start: false,
    showStart: true,    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      openid: users.userInfo.openid,
    });
    wx.showLoading({ title: "加载中...",mask:true })    
    this.selectTixian({openid:users.userInfo.openid})
  },
  // 获取提现数据
  selectTixian:function(data){
    baseSet.selectTixian(data,res=>{
      wx.hideLoading()      
      if(res.data.code == 0){
        var data = res.data.data;
        for (var i = 0; i < data.length; i++) {
          for (var e = 0; e < data[i].array.length; e++) {
            data[i].array[e].time = util.formatTime(data[i].array[e].update_time * 1000, 'yyyy-MM-dd hh:mm',1);
          }
        }
        this.setData({
          list:data,
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../../img/share-index.png',
    }
  }
})