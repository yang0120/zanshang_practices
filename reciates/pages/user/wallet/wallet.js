const app = getApp()
const baseSet = app.baseSet().Interface;
const users = app.globalData;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    price:0,
    bank:"",
    modalStart:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this = this;
    if (users.userInfo){
      this.setData({
        user: users.userInfo
      })
      this.getPrice();
    }else{
      wx.showLoading({ title: "加载中...", mask: true })
      app.getInfo(function (res) {
        if (res) {
          this.setData({
            user: res,
          })
          this.getPrice();
        } else {
          wx.hideLoading()
          // 没有授权，弹框授权
          _this.setData({
            modalStart: true,
          })
        }
      })
    }
    
  },
  // 收支明细
  detailBtn:function(){ 
    wx.navigateTo({
      url: 'detail/detail',
    })
  },
  // 提现明细
  withdBtn:function(){
    wx.navigateTo({
      url: 'withd/withd',
    })
  },
  // 提现
  rawalBtn:function(){
    if(!this.data.bank){
      wx.showModal({
        title:"提示",
        content:"您当前还未添加银行卡，请先添加银行卡",
        showCancel:false,
        success:res=>{
          console.log(res)
        }
      })
      return;
    }
    wx.navigateTo({
      url: 'rawal/rawal',
    })
  },
  getPrice: function () {
    baseSet.getPrice({ openid: users.userInfo.openid }, res => {
      wx.hideLoading();      
      // console.log(res)
      this.setData({
        price: res.data.data.price,
        bank: res.data.data.bank
      })
    })
  },
  // 授权登录获取用户信息
  userInfo: function (e) {
    var _this = this;
    if (e.detail.errMsg == "getUserInfo:ok") {
      wx.showLoading({
        title: '加载中...',
        mask: true,
      })
      _this.setData({
        modalStart: false,
      })
      app.getInfo(function (res) {
        if (res) {
          _this.setData({
            user: res,
          })
          _this.getPrice();
        } else {
          wx.hideLoading()
          // 没有授权，弹框授权
          _this.setData({
            modalStart: true,
          })
        }
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../img/share-index.png',
    }
  }
})