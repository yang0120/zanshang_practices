const app = getApp()
const baseSet = app.baseSet().Interface;
const users = app.globalData;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    config:"",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // this.getConfig(); 
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getConfig();
  },

  other: function () {
    wx.navigateTo({
      url: '../other/other',
    })
  },
  time: function () {
    wx.navigateTo({
      url: '../time/time',
    })
  },
  show: function () {
    wx.navigateTo({
      url: '../show/show',
    })
  },
  setup: function () {
    wx.navigateTo({
      url: '../push/push',
    })
  },
  
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  getConfig: function () {
    wx.showLoading({ title: "加载中...", mask: true })    
    baseSet.getConfig({ openid: users.userInfo.openid }, res => {
      wx.hideLoading();
      if (res.data.code == 0) {
        this.setData({
          config: res.data.data,
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../../img/share-index.png',
    }
  }
})