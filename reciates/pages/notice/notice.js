const app = getApp()
const baseSet = app.baseSet().Interface;
const util = app.util();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    datas:"",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getPush(options.id)
  },
  getPush:function(id){
    baseSet.getPush({id:id},res=>{
      if(res.data.code == 0){
        var data = res.data.data[0];
        data.time = util.formatTime(data.created_time * 1000, 'yyyy-MM-dd hh:mm', 1)
        console.log(data)
        this.setData({
          datas:data,
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: app.globalData.title,
      path: '/pages/index/index',
      imageUrl: '../../img/share-index.png',
    }
  }
})