// 时间戳转时间格式
const formatTime = (tmp, fmt , ise) => {
  var date = null;
  // console.log(tmp);
  // if(ise){
  //   var tmp = tmp - 8*60*60*1000;
  // }
  if (!tmp) {
    return "";
  }
  if (tmp instanceof Date) {
    date = tmp;
  } else if (typeof tmp == "string" || typeof tmp == "number") {
    date = new Date(tmp);
  } else {
    return "";
  }
  if (date == "Invalid Date") {
    var aaa = tmp.replace(/-/g, "/");
    date = new Date(aaa);
  }
  if (!fmt) {
    fmt = "yyyy-MM-dd";
  }
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
  }
  var o = {
    "M+": date.getMonth() + 1, //月份
    "d+": date.getDate(), //日
    "h+": date.getHours(), //小时
    "m+": date.getMinutes(), //分
    "s+": date.getSeconds(), //秒
    "q+": Math.floor((date.getMonth() + 3) / 3), //季度
    "S": date.getMilliseconds() //毫秒
  };
  for (var k in o) {
    if (new RegExp("(" + k + ")").test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    }
  }
  return fmt;
  // 'yyyy-MM-dd hh:mm:ss'
}


// 时间转时间戳格式
const timeFormat = res => {
  var str = res.replace(/-/g, '/');
  var date = Math.round(new Date(str).getTime() / 1000);
  return date;
}
// 金额对象转数组
const objArray = res => {
  let arr = []
  for(var i=1; i<=9; i++){
    if (res["amount_number" + i]){
      arr.push({ price: res["amount_number" + i], focus: false, num: 60 });                      
    }else{
      return arr;
    }
  }
  return arr;
}
// 金额数组转对象
const arrayObj = res =>{
  let obj = {}
  for(var i=1; i<=9; i++){
    if (res[i - 1]){
      var num = parseFloat(res[i - 1].price)
      obj["amount_number" + i] = num;
      if (((Math.round(num * 100)).toString()).indexOf(".") != -1 || num == 0){
        return false;
      }      
    }else{
      obj["amount_number" + i] = 0;            
    }
  }
  return obj;
}
// 判断金额是否有重复
const repeat = res =>{
  for(var i = 0; i<res.length; i++){
    for(var e = 0; e <res.length; e++){
      if(i != e){
        if (res[i].price == res[e].price){
          return false;
        }
      }
    }
  }
  return true;
}
// 存储导出数据名称
const storage = data =>{
  var arr = [];
  wx.getStorage({
    key: 'data',
    success: function (res) {
      if(res.data){
        var arr = JSON.parse(res.data);
        arr.push(data)
        var str = JSON.stringify(arr);
        wx.setStorage({
          key: "data",
          data: str
        })
      }
    },
    fail:res=>{
      arr.push(data)
      var str = JSON.stringify(arr);
      wx.setStorage({
        key: "data",
        data: str
      })
    }
  })

}
// 按时间倒序排列
const compare = (prop) => {
  return function (obj1, obj2) {
    var val1 = obj1[prop];
    var val2 = obj2[prop];
    if (!isNaN(Number(val1)) && !isNaN(Number(val2))) {
      val1 = Number(val1);
      val2 = Number(val2);
    }
    if (val1 < val2) {
      return 1;
    } else if (val1 > val2) {
      return -1;
    } else {
      return 0;
    }
  }
}
module.exports = {
  formatTime: formatTime,
  objArray:objArray,
  arrayObj:arrayObj,
  timeFormat:timeFormat,
  repeat: repeat,
  storage: storage,
  compare: compare,
}
