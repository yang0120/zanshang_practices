
// var base = 'http://127.0.0.1:8371/app/';
// var base = "https://www.leinhome.com/app/";

// var base = 'http://gfdhgn.cn:8371/app/';
var base = 'https://gfdhgn.cn/app/';
// var base = "https://118.24.183.197/app/";

var util = require("util.js");
var Interface = {
  // 登录
  login: function (data, suc) {
    return wx.request({
      url: base + 'user/login',
      data: data,
      method: "POST",
      success: suc,
      fail: function (res) {
        console.log(res)
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 赞赏码添加
  add: function (data, suc) {
    return wx.request({
      url: base + 'appreciates/add',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 修改赞赏码
  update: function (data, suc) {
    return wx.request({
      url: base + 'appreciates/update',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 获取赞赏码信息
  select: function (data, suc) {
    return wx.request({
      url: base + 'appreciates/select',
      data: data,
      method: "POST",
      success: suc,
      fail: function (res) {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },

  // 删除赞赏码
  deleteAppreciates: function (data, suc) {
    return wx.request({
      url: base + 'appreciates/deleteAppreciates',
      data: data,
      method: "POST",
      success: suc,
      fail: function (res) {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  //粉丝列表
  getFans: function (data, suc) {
    return wx.request({
      url: base + 'fans/getFans',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 支付
  fansAdd: function (data, suc) {
    return wx.request({
      url: base + 'fans/add',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 粉丝浏览量
  badd: function (data, suc) {
    return wx.request({
      url: base + 'fans/addBrowse',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  //收支记录
  getpayment: function (data, suc) {
    return wx.request({
      url: base + 'fans/getpayment',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  //粉丝备注
  updateName: function (data, suc) {
    return wx.request({
      url: base + 'fans/updateName',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  //获取是否弹框
  ifmassage: function (data, suc) {
    return wx.request({
      url: base + 'fans/ifmassage',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 用户设置
  getConfig: function (data, suc) {
    return wx.request({
      url: base + 'user/getConfig',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 获取钱包
  getPrice: function (data, suc) {
    return wx.request({
      url: base + 'user/getPrice',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 用户设置修改
  updateConf: function (data, suc) {
    return wx.request({
      url: base + 'user/updateConf',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 添加formid
  addform: function (data, suc) {
    return wx.request({
      url: base + 'user/addform',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 发送通知价格
  getPushServe: function (data, suc) {
    return wx.request({
      url: base + 'pushServe/getPushServe',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 重置
  reset: function (data, suc) {
    return wx.request({
      url: base + 'pushServe/reset',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 发送通知
  pushAdd: function (data, suc) {
    return wx.request({
      url: base + 'pushServe/add',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 通知列表
  findPush: function (data, suc) {
    return wx.request({
      url: base + 'pushServe/findPush',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 删除通知
  deletes: function (data, suc) {
    return wx.request({
      url: base + 'pushServe/delete',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 通知详情
  getPush: function (data, suc) {
    return wx.request({
      url: base + 'pushServe/getPush',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 查询粉丝收到的通知
  userIdPush: function (data, suc) {
    return wx.request({
      url: base + 'pushServe/userIdPush',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 查询银行卡
  bankFind: function (data, suc) {
    return wx.request({
      url: base + 'bank/findBank',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 添加银行卡
  bankAdd: function (data, suc) {
    return wx.request({
      url: base + 'bank/add',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 修改银行卡
  bankUpdate: function (data, suc) {
    return wx.request({
      url: base + 'bank/update',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 申请提现
  getBank: function (data, suc) {
    return wx.request({
      url: base + 'bank/getBank',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 提现列表
  selectTixian: function (data, suc) {
    return wx.request({
      url: base + 'bank/selectTixian',
      data: data,
      method: "POST",
      success: suc,
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  },
  // 下载图片
  downImg:function(url,suc){
    return wx.downloadFile({
      url:url,
      success: function (res) {
        console.log(res)
        let path = res.tempFilePath
        wx.saveImageToPhotosAlbum({
          filePath: path,
          success:suc,
          fail(res) {
            wx.hideLoading();
            wx.showToast({
              title: "保存失败"
            })
          }
        })
      }, fail: function (res) {
        wx.hideLoading();
        wx.showToast({
          title: "网络错误"
        }) 
      }
    })
  },
  // 下载文件
  downFike: function (url, title) {
    return wx.downloadFile({
      url:url,
      success: function (res) {
        console.log(res)
        let path = res.tempFilePath
        wx.saveFile({
          tempFilePath: path,
          success: e=>{
            util.storage({ url: e.savedFilePath,title:title});
            wx.showModal({
              title: '提示',
              content: '保存成功,是否打开查看',
              success: function (res) {
                if (res.confirm) {
                  wx.openDocument({
                    filePath: e.savedFilePath,
                    fileType: "xlsx",
                    success: function (res) {
                      console.log('打开文档成功')
                    }
                  })
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          },
          fail(res) {
            wx.hideLoading();
            wx.showToast({
              title: "保存失败"
            })
          }
        })
      }, fail: function (res) {
        wx.hideLoading();
        wx.showToast({
          title: "网络错误"
        })
      }
    })
  }
}
module.exports = {
  Interface: Interface,
}