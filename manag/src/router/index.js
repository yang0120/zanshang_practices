import Vue from 'vue'
import Router from 'vue-router'
import login from '@/components/login'
import index from '@/components/index'
import rawal from "@/components/rawal"
import money from "@/components/money"
import plan from "@/components/plan"
import account from "@/components/account"
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: login,
    },
    {
      path: '/rawal',
      name: 'index',
      component: index,
      children:[
        {
          path:'/',
          name:"rawal",
          component:rawal,
        },
        {
          path:"/money",
          name:"money",
          component:money,
        },
        {
          path:"/plan",
          name:"plan",
          component:plan,
        },
        {
          path:"/account",
          name:"account",
          component:account,
        }
      ],
    },
  ]
})
