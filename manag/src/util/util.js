// import { baseUrl } from '@/config/api'

var baseUrl = "https://www.leinhome.com/app/";
// var baseUrl = "https://gfdhgn.cn/app/";

import Ajax from './ajax'

export const ajax = (params) => {
	var defaults = {
		url: baseUrl + params.url,
		dataType:params.dataType? params.dataType:"json",
		contentType:params.contentType?params.contentType:"application/x-www-form-urlencoded",
		type: params.type ? params.type : "post",
		async: String(params.async) ? params.async : true,
	};

	for(var key in defaults) {
		params[key] = defaults[key];
	}
	var _successFn = params.success;
	var _error = params.error;
	params.beforeSend = function(req) {

	}
	params.success = function(result, status, xhr) {
		if(!result.code || result.code == 0) {
			_successFn(result, status, xhr)
		} else {
			if(result.code == 1027){
				myApp.$message({
					type:"warning",
					message:"当前导出数据为空",
				});
				return;
			}
			myApp.$message({
				type:"warning",
				message:result.msg?result.msg.stateCode:"网络错误",
			});
			if(result.code == 1000){
				window.sessionStorage.clear();
				myApp.$router.push('/');			
			}
		}
	}
	params.error = function(err) {
		myApp.$message({
			type:"warning",
			message:err?err:"请求超时！",
		});
	}
	Ajax(params);
}

export const dateFormat = (tmp, fmt) => {
	var date = null;
	if(!tmp) {
		return "";
	}
	if(tmp instanceof Date) {
		date = tmp;
	} else if(typeof tmp == "string" || typeof tmp == "number") {
		date = new Date(tmp);
	} else {
		return "";
	}
	if(date == "Invalid Date") {
		var aaa = tmp.replace(/-/g, "/");
		date = new Date(aaa);
	}
	if(!fmt) {
		fmt = "yyyy-MM-dd";
	}
	if(/(y+)/.test(fmt)) {
		fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
	}
	var o = {
		"M+": date.getMonth() + 1, //月份
		"d+": date.getDate(), //日
		"h+": date.getHours(), //小时
		"m+": date.getMinutes(), //分
		"s+": date.getSeconds(), //秒
		"q+": Math.floor((date.getMonth() + 3) / 3), //季度
		"S": date.getMilliseconds() //毫秒
	};
	for(var k in o) {
		if(new RegExp("(" + k + ")").test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
		}
	}
	return fmt;
}