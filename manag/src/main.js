// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import '../node_modules/element-ui/lib/theme-chalk/index.css'

import '../node_modules/_bootstrap@3.3.7@bootstrap/js/dropdown.js'
import '../node_modules/_bootstrap@3.3.7@bootstrap/dist/css/bootstrap.css'


Vue.config.productionTip = false
Vue.use(ElementUI)
/* eslint-disable no-new */
window.myApp = new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
